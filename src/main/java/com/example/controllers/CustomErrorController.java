package com.example.controllers;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {

	private static final String PATH = "/error";
	   	
	@Override
	public String getErrorPath() {
		return PATH;
	}
		
	@RequestMapping(value="/403")
	public String error403() {
	     return "403";
	}
	
	@RequestMapping(value="/404")
	public String error404() {
	     return "404";
	}
}
