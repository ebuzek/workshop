package com.example.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {

	@RequestMapping("/page")
	public String page(@AuthenticationPrincipal User customUser) {
		System.out.println("custome: " + customUser.getUsername());
		return "page";
	}
	
	@RequestMapping("/admin/page")
	public String adminPage() {
		return "adminPage";
	}
	
	@RequestMapping("/login")
	public String login() {
		System.out.println("xxx");
		return "login";
	}
	
	@RequestMapping("/logout")
	public String logout() {
		System.out.println("xxx");
		return "login";
	}
	
}
