package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.jpa.PersonRepository;
import com.example.model.Person;

@Controller
public class DreamsystemController {

	@Autowired
	private PersonRepository personRepository;
	
	@RequestMapping("/")
	public String index() {
		return "home";
	}
	
	@RequestMapping("/home")
	public String home() {
		return "home";
	}
	
	@RequestMapping("/people")
	public String people(Model model) {
		Iterable<Person> persons = personRepository.findAll();
		model.addAttribute("persons", persons);
		return "people";
	}
	

	
}
