package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.business.Engine;
import com.example.jpa.PersonRepository;
import com.example.model.Person;

@Controller
public class PersonController {

	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	Engine engine;
	
	@RequestMapping("/person")
	public String person(Model model) {
		Iterable<Person> persons = engine.getOld();
		for (Person p : persons) {
			System.out.println(" > !ahoj Person " + p.name);
		}
		model.addAttribute("persons", persons);
		return "person";
	}
		
}
