package com.example.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.jpa.PersonRepository;
import com.example.model.Person;

@Component
public class Engine {

	@Autowired
	private PersonRepository personRepository;
	
	public List<Person> getOld() {
		return personRepository.findByAge(42);
	}
	
	
}
